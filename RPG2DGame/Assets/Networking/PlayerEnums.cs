using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PlayerEnums
{
    Idle,
    Run,
    Attack,
    Jump,
    Sit,
    Die
}
