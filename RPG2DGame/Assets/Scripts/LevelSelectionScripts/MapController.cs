﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapController : MonoBehaviour
{
    float timer;
    public List<Transform> nodes;
    static Vector3 currentPositionHolder;
    int currentNode;
    float moveSpeed;
    MysteriousMainCharacter player;
    GameObject playerObject;
    public bool moveLeft, moveRight;

    private void Start()
    {
        moveLeft = false;
        moveRight = false;
        playerObject = GameObject.Find("MysteriousMainCharacterSword(Clone)");
        player = playerObject.GetComponent<MysteriousMainCharacter>();
        moveSpeed = player.MoveSpeed;
        timer = Time.deltaTime * moveSpeed;
        currentNode = 0;
        playerObject.transform.position = nodes[0].position;
    }
    private void Update()
    {
        if (moveLeft)
        {
            timer += Time.deltaTime * moveSpeed;
            if (playerObject.transform.position != currentPositionHolder)
            {
                playerObject.transform.position = Vector3.Lerp(playerObject.transform.position, currentPositionHolder, timer);
            }
            else
            {
                if (currentNode < nodes.Count - 1)
                {
                    currentNode++;
                    CheckNode();
                }
                else
                {
                    moveLeft = false;
                    player.m_Anim.SetFloat("Run", 0f);
                }
            }
        }
        else if(moveRight)
        {
            timer += Time.deltaTime * moveSpeed;
            if (playerObject.transform.position != currentPositionHolder)
            {
                playerObject.transform.position = Vector3.Lerp(playerObject.transform.position, currentPositionHolder, timer);
            }
            else
            {
                if (currentNode > 0)
                {
                    currentNode--;
                    CheckNode();
                }
                else
                {
                    moveRight = false;
                    player.m_Anim.SetFloat("Run", 0f);
                }
            }
        }
    }

    void CheckNode()
    {
        timer = 0;
        currentPositionHolder = nodes[currentNode].transform.position;
    }
}
