﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Advertisements;

public class AdsManager : MonoBehaviour, IUnityAdsListener
{
    string gameId = "4212011";
    string myPlacementId = "rewardedVideo";
    bool testMode = true;

    public static AdsManager instance;

    public void Start()
    {
        instance = this;
        DontDestroyOnLoad(gameObject);

        Advertisement.AddListener(this);
        Advertisement.Initialize(gameId, testMode);
    }

    public void DisplayAd()
    {
        Advertisement.Show(myPlacementId);
    }
    public void OnUnityAdsDidFinish(string placementId, ShowResult showResult)
    {
        // Define conditional logic for each ad completion status:
        if (showResult == ShowResult.Finished)
        {
            GameManager.instance.player.AddGems(100);
            UIManager.Instance.OpenShop(GameManager.instance.player.diamonds);
            // Reward the user for watching the ad to completion.
        }
        else if (showResult == ShowResult.Skipped)
        {
            Debug.Log("You skipped the ad! No Gems for you!");
            // Do not reward the user for skipping the ad.
        }
        else if (showResult == ShowResult.Failed)
        {
            Debug.LogWarning("The ad did not finish due to an error.");
        }
    }

    public void OnUnityAdsReady(string placementId)
    {
    }

    public void OnUnityAdsDidError(string message)
    {
        // Log the error.
    }

    public void OnUnityAdsDidStart(string placementId)
    {
        // Optional actions to take when the end-users triggers an ad.
    }
}
