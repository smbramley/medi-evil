﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class Player : MonoBehaviour, IDamageable
{
    //get reference to rigidbody
    private new Rigidbody2D rigidbody2D;
    [SerializeField] private float jumpForce = 5.0f;
    //[SerializeField] private LayerMask groundLayer;
    // Start is called before the first frame update
    private bool resetJump = false;
    [SerializeField] private float speed = 5.0f;
    private bool grounded = false;
    private PlayerAnimation playerAnimation;
    private SpriteRenderer playerSpriteRenderer;
    private SpriteRenderer swordArcSprite;
    public List<SpriteRenderer> sprites;
    private List<bool> currentWeapon;

    public int diamonds;
    void Start()
    {
        rigidbody2D = GetComponent<Rigidbody2D>();
        playerAnimation = GetComponent<PlayerAnimation>();
        playerSpriteRenderer = GetComponentInChildren<SpriteRenderer>();
        swordArcSprite = transform.GetChild(1).GetComponent<SpriteRenderer>();
        Health = 4;
        currentWeapon = new List<bool>() { true, false, false};
    }

    // Update is called once per frame
    void Update()
    {
        Movement();

        if (CrossPlatformInputManager.GetButtonDown("Attack") /*&& IsGrounded()==true*/)
        {
            playerAnimation.Attack();
        }
        //if (CrossPlatformInputManager.GetButtonDown("B_Button"))
        //{
        //    playerAnimation.Jump(true);
        //}
    }

    void Movement()
    {
        //Horizontal input left/right
        float moveHorizontal = CrossPlatformInputManager.GetAxisRaw("Horizontal");
        float moveVertical = CrossPlatformInputManager.GetAxisRaw("Vertical");
        grounded = IsGrounded();

        if (moveHorizontal > 0)
        {
            FlipPlayer(true);
        }
        else if (moveHorizontal < 0)
        {
            FlipPlayer(false);
        }

        if ((Input.GetKeyDown(KeyCode.Space) || CrossPlatformInputManager.GetButtonDown("Jump")) /*&& IsGrounded()==true*/)
        {
            //Debug.Log("Jump!");
            rigidbody2D.velocity = new Vector2(rigidbody2D.velocity.x, jumpForce);
            StartCoroutine(ResetJumpRoutine());
            playerAnimation.Jump(true);
        }


        rigidbody2D.velocity = new Vector2(moveHorizontal * speed, rigidbody2D.velocity.y);
        playerAnimation.Move(moveHorizontal);

    }

    public int Health { get; set; }
    public int GetCurrentWeapon()
    {
        int current = 0;
        foreach(var weapon in currentWeapon)
        {
            if (weapon)
            {
                return current;
            }
            current++;
        }
        return 0;
    }

    public void Damage()
    {
        if(Health < 1) { return; }
        Health--;
        UIManager.Instance.UpdateLives(Health);

        if(Health < 1)
        {
            playerAnimation.Death();
        }
    }

    void FlipPlayer(bool faceRight)
    {
        if (faceRight==true)
        {
            //playerSpriteRenderer.flipX = false;
            swordArcSprite.flipX = false;
            swordArcSprite.flipY = false;

            foreach(var item in sprites)
            {
                if (item.tag == "Weapons")
                {
                    item.transform.rotation = new Quaternion(0f, 0f, -80f, 0f);
                }
                item.flipX = true;
            }

            Vector3 newPos = swordArcSprite.transform.localPosition;
            newPos.x = 1.01f;
            swordArcSprite.transform.localPosition = newPos;
        }
        else if (faceRight == false)
        {
            playerSpriteRenderer.flipX = true;
            //swordArcSprite.flipX = true;
            swordArcSprite.flipY = true;

            foreach (var item in sprites)
            {
                item.flipX = false;
            }

            Vector3 newPos = swordArcSprite.transform.localPosition;
            newPos.x = -1.01f;
            swordArcSprite.transform.localPosition = newPos;
        }
    }

    bool IsGrounded()
    {
        RaycastHit2D hitInfo = Physics2D.Raycast(transform.position, Vector2.up, .002f, 1 << 8);
        Debug.DrawRay(transform.position, Vector2.down,Color.green);

        if (hitInfo.collider != null)
        {
            if (resetJump == false) { playerAnimation.Jump(false); return true; }
        }
        return false;
    }

    IEnumerator ResetJumpRoutine()
    {
        resetJump = true;
        yield return new WaitForSeconds(0.1f);
        resetJump = false;
    }

    public void AddGems(int amount)
    {
        diamonds += amount;
        UIManager.Instance.UpdateGemCount(diamonds);
    }
}
