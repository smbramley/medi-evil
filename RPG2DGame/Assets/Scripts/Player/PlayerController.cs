﻿using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Netcode;
using UnityEngine;

public abstract class PlayerController : NetworkBehaviour
{
    public bool IsSit = false;
    public int currentJumpCount = 0;
    public bool isGrounded = false;
    public bool OnceJumpRayCheck = false;

    public bool Is_DownJump_GroundCheck = false;
    public float m_MoveX;
    public Rigidbody2D m_rigidbody;
    protected CapsuleCollider2D m_CapsulleCollider;
    public Animator m_Anim;
    private Vector3 bottomLeftLimit;
    private Vector3 topRightLimit;
    public bool canMove;

    [Header("[Setting]")]
    public float MoveSpeed = 6;
    public int JumpCount = 2;
    public float jumpForce = 15f;

    public List<AnimationClip> attacksList;
    protected int currentAttack = 0;
    [HideInInspector] public static PlayerController instance;
    [HideInInspector] public string areaTransitionName;

    // Use this for initialization
    void Start()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            if (instance != this)
            {
                Destroy(gameObject);
            }
        }

        DontDestroyOnLoad(gameObject);
    }
    protected void AnimUpdate()
    {
        //if (!m_Anim.GetCurrentAnimatorStateInfo(0).IsName(attacksList[currentAttack].name))
        //{
            if (Input.GetKey(KeyCode.Mouse0))
            {
                m_Anim.Play(attacksList[currentAttack].name);
            }
            else
            {
                if (m_MoveX == 0)
                {
                    if (!OnceJumpRayCheck)
                        m_Anim.Play("Idle");
                }
                else
                {
                    m_Anim.Play("Run");
                }
            }
        //}
    }




    protected void Flip(bool bLeft)
    {
        transform.localScale = new Vector3(bLeft ? 1 : -1, 1, 1);
    }


    protected void prefromJump()
    {
        m_Anim.SetBool("Jumping", true);
        m_Anim.Play("Jump");

        m_rigidbody.velocity = new Vector2(0, 0);

        m_rigidbody.AddForce(Vector2.up * jumpForce, ForceMode2D.Impulse);

        OnceJumpRayCheck = true;
        isGrounded = false;

        currentJumpCount++;
    }

    protected void DownJump()
    {
        if (!isGrounded)
            return;


        if (!Is_DownJump_GroundCheck)
        {
            m_Anim.SetBool("Jumping", true);
            m_Anim.Play("Jump");

            m_rigidbody.AddForce(-Vector2.up * 10);
            isGrounded = false;

            m_CapsulleCollider.enabled = false;

            StartCoroutine(GroundCapsulleColliderTimmerFuc());
        }
    }

    IEnumerator GroundCapsulleColliderTimmerFuc()
    {
        yield return new WaitForSeconds(0.3f);
        m_CapsulleCollider.enabled = true;
    }

    Vector2 RayDir = Vector2.down;


    float PretmpY;
    float GroundCheckUpdateTic = 0;
    float GroundCheckUpdateTime = 0.01f;
    protected void GroundCheckUpdate()
    {
        if (!OnceJumpRayCheck)
            return;

        GroundCheckUpdateTic += Time.deltaTime;

        if (GroundCheckUpdateTic > GroundCheckUpdateTime)
        {
            GroundCheckUpdateTic = 0;

            if (PretmpY == 0)
            {
                PretmpY = transform.position.y;
                return;
            }

            float reY = transform.position.y - PretmpY;

            if (reY <= 0)
            {

                if (isGrounded)
                {
                    LandingEvent();
                    OnceJumpRayCheck = false;
                }
            }
            PretmpY = transform.position.y;
        }
    }
    protected abstract void LandingEvent();

    public void SetBounds(Vector3 botLeft, Vector3 topRight)
    {
        bottomLeftLimit = botLeft + new Vector3(.5f, 1f, 0f);
        topRightLimit = topRight + new Vector3(-.5f, -1f, 0f);
    }
}
































//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;

//public class PlayerController : MonoBehaviour
//{

//    public Rigidbody2D theRB;
//    public float moveSpeed;

//    public Animator myAnim;

//    public static PlayerController instance;

//    public string areaTransitionName;
//    private Vector3 bottomLeftLimit;
//    private Vector3 topRightLimit;

//    public bool canMove = true;

//    // Use this for initialization
//    void Start()
//    {
//        if (instance == null)
//        {
//            instance = this;
//        }
//        else
//        {
//            if (instance != this)
//            {
//                Destroy(gameObject);
//            }
//        }

//        DontDestroyOnLoad(gameObject);
//    }

//    // Update is called once per frame
//    void Update()
//    {
//        if (canMove)
//        {
//            Input.GetKeyDown("Space");
//            theRB.velocity = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical")) * moveSpeed;

//        }
//        else
//        {
//            theRB.velocity = Vector2.zero;
//        }


//        myAnim.SetFloat("moveX", theRB.velocity.x);
//        myAnim.SetFloat("moveY", theRB.velocity.y);

//        if (Input.GetAxisRaw("Horizontal") == 1 || Input.GetAxisRaw("Horizontal") == -1 || Input.GetAxisRaw("Vertical") == 1 || Input.GetAxisRaw("Vertical") == -1)
//        {
//            if (canMove)
//            {
//                myAnim.SetFloat("lastMoveX", Input.GetAxisRaw("Horizontal"));
//                myAnim.SetFloat("lastMoveY", Input.GetAxisRaw("Vertical"));
//            }
//        }

//        transform.position = new Vector3(Mathf.Clamp(transform.position.x, bottomLeftLimit.x, topRightLimit.x), Mathf.Clamp(transform.position.y, bottomLeftLimit.y, topRightLimit.y), transform.position.z);
//    }

//    public void SetBounds(Vector3 botLeft, Vector3 topRight)
//    {
//        bottomLeftLimit = botLeft + new Vector3(.5f, 1f, 0f);
//        topRightLimit = topRight + new Vector3(-.5f, -1f, 0f);
//    }
//}
