﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AudioManager : MonoBehaviour {

    public AudioClip[] sfx;
    public AudioClip[] bgm;

    public static AudioManager instance;

    // Use this for initialization
    void Start () {
        instance = this;

        DontDestroyOnLoad(this.gameObject);
	}
	
	// Update is called once per frame
	void Update () {
        if(SceneManager.GetActiveScene().name == "MainMenu")
        {
            //PlayBGM(0);
        }
        else if(SceneManager.GetActiveScene().name == "Home")
        {
            //PlayBGM(5);
        }
	}

    public void PlaySFX(int soundToPlay)
    {
        AudioSource audioSource = GetComponent<AudioSource>();
        if (soundToPlay < sfx.Length)
        {
            audioSource.clip = sfx[soundToPlay];
            audioSource.Play();
        }
    }

    public void PlayBGM(int musicToPlay)
    {
        AudioSource audioSource = GetComponent<AudioSource>();
        if (!bgm[musicToPlay] == audioSource.isPlaying)
        {
            StopMusic();

            if (musicToPlay < bgm.Length)
            {
                audioSource.clip = bgm[musicToPlay];
                audioSource.Play();
            }
        }
    }

    public void StopMusic()
    {
        AudioSource audioSource = GetComponent<AudioSource>();
        for (int i = 0; i < bgm.Length; i++)
        {
            audioSource.Stop();
        }
    }

    public void PlayMusicPassedIn(AudioClip clip)
    {
        AudioSource audioSource = GetComponent<AudioSource>();
        audioSource.clip = clip;
        audioSource.Play();
    }
}
