﻿using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class EssentialsLoader : MonoBehaviour
{

    public GameObject UIScreen;
    public GameObject player;
    public GameObject gameMan;
    public GameObject audioMan;
    public GameObject battleMan;
    public GameObject cameraManager;
    public GameObject cameraCMCam;
    public GameObject adsManager;
    public GameObject networkManager;


    public static EssentialsLoader instance;

    bool checkPlayer;

    /// <summary>
    /// 
    /// </summary>
    void Awake()
    {
        KeepSingleton();
    }

    /// <summary>
    /// 
    /// </summary>
    private void Update()
    {
        if (player != null)
        {
            if (PlayerController.instance == null)
            {
                PlayerController.instance = Instantiate(player).GetComponent<PlayerController>();
                if (CameraController.instance == null)
                {
                    CameraController.instance = Instantiate(cameraManager).GetComponent<CameraController>();
                }
                if (CameraController1.instance == null)
                {
                    CameraController1.instance = Instantiate(cameraCMCam).GetComponent<CameraController1>();
                }
                cameraCMCam.GetComponent<CinemachineVirtualCamera>().Follow = PlayerController.instance.transform;
                //if (GameObject.Find("Bounds"))
                //    if (GameObject.Find("Bounds").GetComponent<Collider2D>())
                //        cameraCMCam.GetComponent<CinemachineConfiner>().m_BoundingShape2D = GameObject.Find("Bounds").GetComponent<Collider2D>();
                if (player.GetComponent<MysteriousMainCharacter>())
                    player.GetComponent<MysteriousMainCharacter>().checkDontDestroy = true;
            }
        }

        if (AdsManager.instance == null)
            AdsManager.instance = Instantiate(adsManager).GetComponent<AdsManager>();
        else if (AdsManager.instance)
            if (GameObject.Find("GetCoins"))
                GameObject.Find("GetCoins").GetComponent<Button>().onClick.AddListener(() => AdsManager.instance.DisplayAd());

        if (UICanvas.instance == null)
            UICanvas.instance = Instantiate(UIScreen).GetComponent<UICanvas>();

        if (GameManager.instance == null)
            GameManager.instance = Instantiate(gameMan).GetComponent<GameManager>();

        if (AudioManager.instance == null)
            AudioManager.instance = Instantiate(audioMan).GetComponent<AudioManager>();

        if (BattleManager.instance == null)
            BattleManager.instance = Instantiate(battleMan).GetComponent<BattleManager>();
    }

    public static void AddPlayer(GameObject _player)
    {
        instance.player = _player;
    }

    /// <summary>
    /// 
    /// </summary>
    public void KeepSingleton()
    {
        if (instance == null)
        {
            DontDestroyOnLoad(gameObject);
            instance = this;
        }
        else
        {
            if (instance != this)
            {
                Destroy(gameObject);
            }
        }
    }

}
