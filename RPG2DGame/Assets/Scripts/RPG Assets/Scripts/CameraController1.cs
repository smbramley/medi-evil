﻿using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class CameraController1 : MonoBehaviour {

    public Transform target;
    public static CameraController1 instance;
    //public Tilemap theMap;
    private Vector3 bottomLeftLimit;
    private Vector3 topRightLimit;

    private float halfHeight;
    private float halfWidth;

    public int musicToPlay;
    private bool musicStarted;

	// Use this for initialization
	void Awake () 
    {
        instance = this;
        DontDestroyOnLoad(gameObject);

        //target = PlayerController.instance.transform;
        //if (FindObjectOfType<MysteriousMainCharacter>())
        //{
        //    target = FindObjectOfType<MysteriousMainCharacter>().transform;
        //}

        //halfHeight = Camera.main.orthographicSize;
        //halfWidth = halfHeight * Camera.main.aspect;

        //theMap.CompressBounds();
        //bottomLeftLimit = theMap.localBounds.min + new Vector3(halfWidth, halfHeight, 0f);
        //topRightLimit = theMap.localBounds.max + new Vector3(-halfWidth, -halfHeight, 0f);
        //bottomLeftLimit = theMap.localBounds.min;
        //topRightLimit = theMap.localBounds.max;

        //MysteriousMainCharacter.instance.SetBounds(theMap.localBounds.min, theMap.localBounds.max);
    }

    public void Update()
    {
        if (!(gameObject.GetComponent<CinemachineVirtualCamera>().Follow))
        {
            gameObject.GetComponent<CinemachineVirtualCamera>().Follow = GameObject.FindGameObjectWithTag("Player").transform;
        }
    }

    // LateUpdate is called once per frame after Update
    //void LateUpdate () {
    //       transform.position = new Vector3(target.position.x, target.position.y, transform.position.z);

    //       //keep the camera inside the bounds
    //       transform.position = new Vector3(Mathf.Clamp(transform.position.x, bottomLeftLimit.x, topRightLimit.x), Mathf.Clamp(transform.position.y, bottomLeftLimit.y, topRightLimit.y), transform.position.z);

    //       if(!musicStarted)
    //       {
    //           musicStarted = true;
    //           AudioManager.instance.PlayBGM(musicToPlay);
    //       }
    //}


}
