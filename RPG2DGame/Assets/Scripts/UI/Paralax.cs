﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Paralax : MonoBehaviour
{
    //private float length, startpos;
    public GameObject character;
    public float parallaxEffect;
    public Vector3 moveVector = new Vector3(1f,0f,0f);
    public float vectorSpeed = 3.0f;

    // Start is called before the first frame update
    void Start()
    {
        if(gameObject.name=="Boss(Clone)"|| gameObject.name == "Boss")
        {
            gameObject.GetComponent<Animator>().Play("Run_Const");
        }
        //startpos = transform.position.x;
        //length = GetComponent<SpriteRenderer>().bounds.size.x;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        gameObject.transform.Translate(moveVector * vectorSpeed * Time.deltaTime);

        //float temp = (cam.transform.position.x * (1 - parallaxEffect));
        //float distance = (cam.transform.position.x * parallaxEffect);
        //transform.position = new Vector3(startpos + distance, transform.position.y, transform.position.z);

        //if (temp > startpos + length) startpos += length;
        //else if (temp < startpos - length) startpos -= length;
    }
}
