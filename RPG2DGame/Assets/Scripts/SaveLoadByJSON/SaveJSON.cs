﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SaveJSON
{
    public int coinsNum;
    public int diamondsNum;

    public float playerPositionX;
    public float playerPositionY;

    public List<bool> levelsLockedUnlocked = new List<bool>();
    public List<bool> charactersUnlocked = new List<bool>();

    //MARKER If you want to save the enemy position 
    public List<float> enemyPositionX = new List<float>();
    public List<float> enemyPositionY = new List<float>();
    public List<bool> isDead = new List<bool>();

    //MARKER Enemy Health Point
    //public List<int> enemyHps = new List<int>();

}
