﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AreaExitController : MonoBehaviour
{
    public string sceneName;
    public string areaTransitionName;
    public AreaEntrance theEntrance;
    public float waitToLoad = 1f;
    //private bool shouldLoadAfterFade;
    // Start is called before the first frame update
    void Start()
    {
        theEntrance.transitionName = areaTransitionName;
    }

    // Update is called once per frame
    void Update()
    {
        //if (shouldLoadAfterFade)
        //{
        //    waitToLoad -= Time.deltaTime;
        //    if(waitToLoad <= 0)
        //    {
        //        shouldLoadAfterFade = false;
        //        SceneManager.LoadScene(sceneName);
        //    }
        //}
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            //shouldLoadAfterFade = true;
            GameManager.instance.fadingBetweenAreas = true;
            //UIFade.instance.FadeToBlack();
            Initiate.Fade(sceneName, Color.black, 2.0f);
            PlayerController.instance.areaTransitionName = areaTransitionName;
        }
    }
}
