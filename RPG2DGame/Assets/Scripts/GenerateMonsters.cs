﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenerateMonsters : MonoBehaviour
{
    public List<GameObject> monsterList;
    public float waitTime = 100f;

    private void Start()
    {
        StartCoroutine(LoadMonster());
    }

    IEnumerator LoadMonster()
    {
        while (true)
        {
            System.Random rand = new System.Random();
            int randomInt = rand.Next(0, 7);
            GameObject newMonster = Instantiate(monsterList[randomInt]);
            newMonster.transform.position = gameObject.transform.position;

            yield return new WaitForSeconds(waitTime);
        }
    }
}
