﻿using System.Collections;
using System.Collections.Generic;
using Unity.Collections;
using Unity.Netcode;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MysteriousMainCharacter : PlayerController
{
    bool isMapScene, isMainMenu;
    public bool checkDontDestroy = false;
    public GameObject currentWeapon;
    private NetworkVariable<MyCustomData> randomNumber = new NetworkVariable<MyCustomData>(new MyCustomData { _int = 56, _bool = true, },NetworkVariableReadPermission.Everyone, NetworkVariableWritePermission.Owner);
    [SerializeField] private Transform spawnedObjectPrefab;

    [SerializeField] private NetworkVariable<Vector3> networkPositionDirection = new NetworkVariable<Vector3>();
    [SerializeField] private NetworkVariable<Vector3> networkRotationDirection = new NetworkVariable<Vector3>();
    [SerializeField] private NetworkVariable<PlayerEnums> networkPlayerState = new NetworkVariable<PlayerEnums>();
    private CharacterController characterController;
    
    // client caches positions
    private Vector3 oldInputPosition = Vector3.zero;
    private Vector3 oldInputRotation = Vector3.zero;
    private PlayerEnums oldPlayerState = PlayerEnums.Idle;

    public struct MyCustomData : INetworkSerializable
    {
        public int _int;
        public bool _bool;
        public FixedString128Bytes message;

        public void NetworkSerialize<T>(BufferSerializer<T> serializer) where T : IReaderWriter
        {
            serializer.SerializeValue(ref _int);
            serializer.SerializeValue(ref _bool);
            serializer.SerializeValue(ref message);
        }
    }
    private void Start()
    {
        m_CapsulleCollider = this.transform.GetComponent<CapsuleCollider2D>();
        if (!m_Anim)
            m_Anim = transform.Find("model").GetComponent<Animator>();

        m_rigidbody = transform.GetComponent<Rigidbody2D>();
    }



    private void Update()
    {
        if (checkDontDestroy)
        {
            DontDestroyOnLoad(gameObject);
            checkDontDestroy = false;
        }

        if (SceneManager.GetActiveScene().name == "LevelMap")
        {
            isMapScene = true;
            isMainMenu = false;
        }
        else if (SceneManager.GetActiveScene().name == "MainMenu" || SceneManager.GetActiveScene().name == "CharacterSelection")
        {
            isMainMenu = true;
            isMapScene = false;
        }
        else
        {
            isMainMenu = false;
            isMapScene = false;
        }

        if (!isMapScene && !isMainMenu)
        {
            gameObject.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;
            checkInput();

            if (m_rigidbody.velocity.magnitude > 30)
            {
                m_rigidbody.velocity = new Vector2(m_rigidbody.velocity.x - 0.1f, m_rigidbody.velocity.y - 0.1f);

            }
        }
        else if (isMapScene)
        {
            gameObject.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Static;
            checkMapInput();
        }
    }

    public void MapMove(Vector3 nextPos)
    {
        gameObject.transform.position = Vector3.Lerp(gameObject.transform.position, nextPos, MoveSpeed * Time.deltaTime);
    }

    public void checkMapInput()
    {
        if (Input.GetKeyDown(KeyCode.A))
        {
            Flip(false);
            m_MoveX = Input.GetAxis("Horizontal");
            m_Anim.SetFloat("Run", m_MoveX);
            if (!GameObject.Find("Map").GetComponent<MapController>().moveLeft)
                GameObject.Find("Map").GetComponent<MapController>().moveLeft = true;
        }
        if (Input.GetKeyDown(KeyCode.D))
        {
            Flip(true);
            m_MoveX = Input.GetAxis("Horizontal");
            m_Anim.SetFloat("Run", m_MoveX);
            if (!GameObject.Find("Map").GetComponent<MapController>().moveRight)
                GameObject.Find("Map").GetComponent<MapController>().moveRight = true;
        }
    }

    private void ClientMoveAndRotate()
    {
        if (networkPositionDirection.Value != Vector3.zero)
        {
            characterController.SimpleMove(networkPositionDirection.Value);
        }
        if (networkRotationDirection.Value != Vector3.zero)
        {
            transform.Rotate(networkRotationDirection.Value, Space.World);
        }
    }

    private void ClientVisuals()
    {
        if (oldPlayerState != networkPlayerState.Value)
        {
            oldPlayerState = networkPlayerState.Value;
            m_Anim.Play($"{networkPlayerState.Value}");
        }
    }

    public void checkInput()
    {
        if (Input.GetKeyDown(KeyCode.Alpha0)) { currentAttack = 0; }
        else if (Input.GetKeyDown(KeyCode.Alpha1)) { currentAttack = 1; }
        else if (Input.GetKeyDown(KeyCode.Alpha2)) { currentAttack = 2; }
        else if (Input.GetKeyDown(KeyCode.Alpha3)) { currentAttack = 3; }
        else if (Input.GetKeyDown(KeyCode.Alpha4)) { currentAttack = 4; }
        else if (Input.GetKeyDown(KeyCode.Alpha5)) { currentAttack = 5; }
        else if (Input.GetKeyDown(KeyCode.Alpha6)) { currentAttack = 6; }
        else if (Input.GetKeyDown(KeyCode.Alpha7)) { currentAttack = 7; }
        else if (Input.GetKeyDown(KeyCode.Alpha8)) { currentAttack = 8; }
        else if (Input.GetKeyDown(KeyCode.Alpha9)) { currentAttack = 9; }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (currentJumpCount < JumpCount)
            {
                DownJump();
            }
        }

        m_MoveX = Input.GetAxis("Horizontal");

        GroundCheckUpdate();

        if (Input.GetKey(KeyCode.Mouse0))
        {
            if (attacksList.Count > 0)
                m_Anim.Play(attacksList[currentAttack].name);
        }
        else
        {
            if (m_MoveX == 0)
            {
                if (!OnceJumpRayCheck)
                {
                    UpdatePlayerStateServerRpc(PlayerEnums.Idle);
                    m_Anim.Play("Idle");
                }
                m_Anim.SetFloat("Run", m_MoveX);
                if (m_MoveX > 0 || m_MoveX < 0)
                {
                    UpdatePlayerStateServerRpc(PlayerEnums.Run);
                    m_Anim.Play("Run");
                }
            }
            else
            {
                m_Anim.SetFloat("Run", m_MoveX);
                if (m_MoveX > 0 || m_MoveX < 0)
                {
                    UpdatePlayerStateServerRpc(PlayerEnums.Run);
                    m_Anim.Play("Run");
                }
            }
        }


        if (Input.GetKey(KeyCode.Alpha1))
        {
            m_Anim.Play("Die");
        }

        //if (!IsOwner) return;

        //Move Left/Right
        if (Input.GetKey(KeyCode.D))
        {
            if (isGrounded)
            {
                if (attacksList.Count > 0)
                    if (m_Anim.GetCurrentAnimatorStateInfo(0).IsName(attacksList[currentAttack].name)) return;

                transform.transform.Translate(Vector2.right * m_MoveX * MoveSpeed * Time.deltaTime);
            }
            else
            {
                transform.transform.Translate(new Vector3(m_MoveX * MoveSpeed * Time.deltaTime, 0, 0));
            }

            if (attacksList.Count > 0)
                if (m_Anim.GetCurrentAnimatorStateInfo(0).IsName(attacksList[currentAttack].name)) return;

            if (!Input.GetKey(KeyCode.A)) Flip(true);
        }
        else if (Input.GetKey(KeyCode.A))
        {
            if (isGrounded)
            {
                if (attacksList.Count > 0)
                    if (m_Anim.GetCurrentAnimatorStateInfo(0).IsName(attacksList[currentAttack].name)) return;

                transform.transform.Translate(Vector2.right * m_MoveX * MoveSpeed * Time.deltaTime);
            }
            else
            {
                transform.transform.Translate(new Vector3(m_MoveX * MoveSpeed * Time.deltaTime, 0, 0));
            }
            if (attacksList.Count > 0)
                if (m_Anim.GetCurrentAnimatorStateInfo(0).IsName(attacksList[currentAttack].name)) return;

            if (!Input.GetKey(KeyCode.D)) Flip(false);

            // left & right rotation
            Vector3 inputRotation = new Vector3(0, Input.GetAxis("Horizontal"), 0);
            // let server know about position and rotation client changes
            if (oldInputPosition != gameObject.transform.position ||
                oldInputRotation != inputRotation)
            {
                oldInputPosition = gameObject.transform.position;
                UpdateClientPositionAndRotationServerRpc(gameObject.transform.position, inputRotation);
            }
        }


        //Jump
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (attacksList.Count > 0)
                if (m_Anim.GetCurrentAnimatorStateInfo(0).IsName(attacksList[currentAttack].name))
                    return;

            if (currentJumpCount < JumpCount)
            {
                if (!IsSit)
                {
                    prefromJump();
                }
                else
                {
                    DownJump();
                }
            }
        }
    }
    protected override void LandingEvent()
    {
        if (!m_Anim.GetCurrentAnimatorStateInfo(0).IsName("Run") /*&& !m_Anim.GetCurrentAnimatorStateInfo(0).IsName(attacksList[currentAttack].name)*/)
        {
            m_Anim.SetBool("Jumping", false);
            m_Anim.Play("Idle");
        }
    }

    public override void OnNetworkSpawn()
    {
        if (!IsLocalPlayer) enabled = false;
        randomNumber.OnValueChanged += (MyCustomData previousValue, MyCustomData newValue) =>
        {

        };
    }

    [ServerRpc]
    public void UpdateClientPositionAndRotationServerRpc(Vector3 newPosition, Vector3 newRotation)
    {
        networkPositionDirection.Value = newPosition;
        networkRotationDirection.Value = newRotation;
    }

    [ServerRpc]
    public void UpdatePlayerStateServerRpc(PlayerEnums state)
    {
        networkPlayerState.Value = state;
    }
}
