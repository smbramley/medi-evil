using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoundsControl : MonoBehaviour
{
    private void Update()
    {
        if (!CameraController1.instance.gameObject.GetComponent<CinemachineConfiner>().m_BoundingShape2D)
            CameraController1.instance.gameObject.GetComponent<CinemachineConfiner>().m_BoundingShape2D = gameObject.GetComponent<PolygonCollider2D>();
    }
}