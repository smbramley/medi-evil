﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class CharacterSelection : MonoBehaviour
{
    private GameObject[] characters;
    private int currentIndex = 0;

    private void Start()
    {
        characters = new GameObject[transform.childCount];
        for (int i = 0; i < transform.childCount; i++)
            characters[i] = transform.GetChild(i).gameObject;

        foreach (GameObject go in characters)
        {
            go.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezePositionY;
            go.SetActive(false);
        }
        characters[0].SetActive(true);
    }

    public void SwitchToCharacter(int check)
    {
        if (check == 0)
        {
            characters[currentIndex].SetActive(false);
            if (currentIndex == 0) { currentIndex = characters.Length - 1; }
            else { currentIndex--; }
            characters[currentIndex].SetActive(true);
        }
        else if (check == 1 && currentIndex != characters.Length - 1)
        {
            characters[currentIndex].SetActive(false);
            if (currentIndex == characters.Length - 1) { currentIndex = 0; }
            else { currentIndex++; }
            characters[currentIndex].SetActive(true);
        }
    }

    public void SelectCharacter()
    {
        PlayerPrefs.SetString("Character", characters[currentIndex].name);
        if (GameObject.Find("UICanvas(Clone)"))
            GameObject.Find("UICanvas(Clone)").gameObject.SetActive(true);
        EssentialsLoader.AddPlayer(Resources.Load("Characters/" + PlayerPrefs.GetString("Character")) as GameObject);
        SceneManager.LoadScene("Home");
    }
}
