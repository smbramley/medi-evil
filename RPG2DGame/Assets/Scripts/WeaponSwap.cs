using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum WEAPON { FISTS = 0, GUN, SWORD }
public class WeaponSwap : MonoBehaviour
{
    private GameObject player, currentOverWeapon;
    private WEAPON weapon;
    private KeyCode key;
    private bool overWeapon;
    public Quaternion rotationOffSet;
    public Vector3 positionOffSet;

    public float throwSpeed = 4;
    public Vector3 launchOffSet;
    public bool thrown;

    [HideInInspector]
    public bool weaponAttached;
    [SerializeField]
    private List<GameObject> buttons;
    private void Start()
    {
        thrown = true;
        overWeapon = false;
        weaponAttached = false;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.E) && overWeapon && !weaponAttached)
        {
            AddWeaponToPlayer();
        }
        if (Input.GetKeyDown(KeyCode.Z) && weaponAttached)
        {
            DropWeapon();
            if (!thrown)
            {
                transform.position += -transform.right * throwSpeed * Time.deltaTime;
            }
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!weaponAttached)
            if (buttons.Count > 0)
            {
                player = collision.gameObject;
                overWeapon = true;
#if PLATFORM_STANDALONE_WIN
                //buttons[0].gameObject.SetActive(true);
#endif
#if ANDROID
               // buttons[1].gameObject.SetActive(true);
#endif
            }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        overWeapon = false;
#if PLATFORM_STANDALONE_WIN
        //buttons[0].gameObject.SetActive(false);
#endif
#if ANDROID
           // buttons[1].gameObject.SetActive(false);
#endif
    }

    public void AddWeaponToPlayer()
    {
        if (player.GetComponent<MysteriousMainCharacter>().currentWeapon)
        {
            Destroy(player.GetComponent<MysteriousMainCharacter>().currentWeapon);
        }
        player.GetComponent<MysteriousMainCharacter>().currentWeapon = gameObject;
        gameObject.transform.parent = GameObject.Find("player_arm_r").transform;
        gameObject.transform.rotation = rotationOffSet; //new Vector3(0, 0, -45f)
        gameObject.transform.position = GameObject.Find("player_arm_r").transform.position;
        //gameObject.transform.position = positionOffSet; //new Vector3(0.03f, 0.05f, 0f)
        weaponAttached = true;
    }

    public void DropWeapon()
    {
        if (thrown)
        {
            var direction = -transform.right + Vector3.up;
            GetComponent<Rigidbody2D>().AddForce(direction * throwSpeed, ForceMode2D.Impulse);
        }
        transform.Translate(launchOffSet);

        gameObject.transform.parent = null;
        //if (!gameObject.GetComponent<Rigidbody2D>())
        // gameObject.AddComponent<Rigidbody2D>();
        player.GetComponent<MysteriousMainCharacter>().currentWeapon = null;
        weaponAttached = false;
    }
}
